module.exports = function (r) {

  if( !r ){
    r =1 ;
  }
  this.raison = r;


  return {
    fibonacci: fibonacci.bind(this),
    numerique: numerique.bind(this)
  }
};


function fibonacci(n) {
  //console.log('calcul de fibonacci de ', n);
  if (n <= 2) {
    return 1;
  } else {
    return fibonacci(n - 2) + fibonacci(n - 1)
  }

}

function numerique(n){
  if(n<=1){
    return this.raison;
  }else{
    return numerique(n-1) + this.raison;
  }
}

